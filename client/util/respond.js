/** Rill v0.0.906 https://www.npmjs.com/package/rill License: MIT*/
"use strict";
var STATUS_CODES, byteLength, emptyStatus, isType, redirectStatus, reg,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

byteLength = require("byte-length");

STATUS_CODES = require("@rill/http").STATUS_CODES;

isType = require("@rill/is-type");

reg = {
  html: /^\s*</,
  cookieOptions: /(domain|path|expires|max-age|httponly|secure)( *= *[^;]*)?/g,
  cookieValues: / *; */
};

redirectStatus = ["300", "301", "302", "303", "305", "307", "308"];

emptyStatus = ["204", "205", "304"];


/*
 * A default middleware that handles sending the response for the request.
 *
 * @param {Object} ctx
 * @param {Object} req
 * @param {Object} res
 */

module.exports = function(ctx, req, res) {
  var body, cookies, headers, options, originalRes, status, statusText;
  if (res.headersSent) {
    return;
  }
  originalRes = res.res, headers = res.headers, status = res.status, statusText = res.statusText, body = res.body;
  if (cookies = ctx.cookies.serialize()) {
    {
      options = (cookies.match(reg.cookieOptions) || []).join("; ");
      if (options) {
        options = "; " + options;
      }
      cookies = cookies.replace(reg.cookieOptions, "").split(reg.cookieValues).filter(Boolean).map(function(cookie) {
        return cookie + options;
      });
    }
    res.headers["set-cookie"] = cookies;
  }
  if (headers["location"] != null) {
    if (indexOf.call(redirectStatus, status) < 0) {
      status = 302;
    }
  }
  if (Number(status) === 404 && (body != null)) {
    status = 200;
  }
  if (statusText == null) {
    statusText = STATUS_CODES[status];
  }
  switch (false) {
    case !(indexOf.call(emptyStatus, status) >= 0 || (body == null)):
      delete headers["content-type"];
      body = void 0;
      break;
    case !isType.String(body):
      if (headers["content-type"] == null) {
        headers["content-type"] = "text/" + (reg.html.test(body) ? "html" : "plain") + "; charset=UTF-8";
      }
      break;
    case !isType.Buffer(body):
      headers["content-length"] = body.length;
      break;
    case !isType.Stream(body):
      if (headers["content-type"] == null) {
        headers["content-type"] = "application/octet-stream";
      }
      break;
    default:
      try {
        body = JSON.stringify(body);
        if (headers["content-type"] == null) {
          headers["content-type"] = "application/json; charset=UTF-8";
        }
      } catch (undefined) {}
  }
  originalRes.writeHead(status, statusText, headers);
  switch (false) {
    case "HEAD" !== req.method:
      originalRes.end();
      break;
    case !isType.Stream(body):
      body.pipe(originalRes);
      break;
    default:
      if (headers["content-length"] == null) {
        headers["content-length"] = byteLength(body);
      }
      originalRes.end(body);
  }
};
