/** Rill v0.0.906 https://www.npmjs.com/package/rill License: MIT*/
"use strict";
var HttpError, STATUS_CODES;

STATUS_CODES = require("@rill/http").STATUS_CODES;

HttpError = function(code, message, meta) {
  this.code = code;
  this.message = message != null ? message : STATUS_CODES[code];
  Error.call(this);
  if (typeof Error.captureStackTrace === "function") {
    Error.captureStackTrace(this, HttpError);
  }
};

HttpError.prototype = new Error;

HttpError.prototype.name = "HttpError";

module.exports = HttpError;
