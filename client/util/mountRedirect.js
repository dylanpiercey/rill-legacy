/** Rill v0.0.906 https://www.npmjs.com/package/rill License: MIT*/
"use strict";

/*
 * Utility to handle redirects inside mounted middleware.
 *
 * @param {Request} req
 * @param {String} path
 */
module.exports = function(req, path) {
  var redirect;
  redirect = req.redirect;
  return function(url, alt) {
    if (url == null) {
      url = "back";
    }
    if (alt == null) {
      alt = "/";
    }
    if (alt[0] === "/") {
      alt = path + alt;
    }
    if (url[0] === "/") {
      url = path + url;
    }
    return redirect.call(req, url, alt);
  };
};
