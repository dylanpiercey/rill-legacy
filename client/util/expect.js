/** Rill v0.0.906 https://www.npmjs.com/package/rill License: MIT*/
"use strict";
var RillError,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

RillError = (function(superClass) {
  extend(RillError, superClass);

  RillError.prototype.name = "Rill-Error";

  function RillError(message1) {
    this.message = message1;
    Error.call(this);
    if (typeof Error.captureStackTrace === "function") {
      Error.captureStackTrace(this, module.exports);
    }
  }

  return RillError;

})(Error);


/*
 * Simple utility that asserts that a condition is true, or throws an error with a message.
 *
 * @param {Boolean*} condition
 * @param {String} message
 */

module.exports = function(condition, message) {
  if (!condition) {
    throw new RillError(message);
  }
};
