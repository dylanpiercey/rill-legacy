/** Rill v0.0.906 https://www.npmjs.com/package/rill License: MIT*/
"use strict";
var Context, HttpError, Rill, compose, expect, http, mountRedirect, pathToReg, respond,
  slice = [].slice,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

pathToReg = require("path-to-regexp");

compose = require("@rill/compose");

expect = require("./util/expect");

respond = require("./util/respond");

mountRedirect = require("./util/mountRedirect");

HttpError = require("./util/httpError");

Context = require("./context");

http = require("@rill/http");

Rill = (function() {

  /*
  	 * Creates an isomorphic Rill application that can listen for browser and server page requests.
  	 *
  	 * @constructor
   */
  var fn1, j, len, method, ref;

  function Rill() {
    if (!(this instanceof Rill)) {
      return new Rill();
    }
    this.stack = [];
  }


  /*
  	 * Starts the server or browser hijacker.
   */

  Rill.prototype.listen = function() {
    var fn, ref, server;
    fn = compose(this.stack);
    return server = (ref = http.createServer(function(req, res) {
      var context, ref1, request, response;
      res.statusCode = 404;
      context = (ref1 = new Context(req, res), request = ref1.request, response = ref1.response, ref1);
      return fn.call(context, request, response).then(function() {
        return respond(context, request, response);
      })["catch"](function(err) {
        if (typeof console.log === "function") {
          console.log("Rill: Unhandled error.");
        }
        return typeof console.error === "function" ? console.error((err != null ? err.stack : void 0) || err) : void 0;
      });
    })).listen.apply(ref, arguments);
  };


  /*
  	 * Utility to throw an http error during a request.
   */

  Rill["throw"] = function(code, message, meta) {
    throw new HttpError(code, message, meta);
  };


  /*
  	 * Throws if a condition failts.
   */

  Rill.assert = function(val, code, message, meta) {
    if (val) {
      return;
    }
    throw new HttpError(code, message, meta);
  };


  /*
  	 * Simple syntactic sugar for functions that wish to modify the current application instance.
  	 *
  	 * @param {Function...} transformers
   */

  Rill.prototype.setup = function() {
    var j, len, transformer, transformers;
    transformers = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    for (j = 0, len = transformers.length; j < len; j++) {
      transformer = transformers[j];
      if (transformer != null) {
        transformer(this);
      }
    }
    return this;
  };


  /*
  	 * Append new middleware to the current rill application stack.
  	 *
  	 * @param {Rill|Function|null...} stack
   */

  Rill.prototype.use = function() {
    var stack;
    stack = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    this.stack = this.stack.concat(stack);
    return this;
  };


  /*
  	 * Attaches a seperate rill application or middleware at a given hostname.
  	 *
  	 * @param {String} host
  	 * @param {Rill|Function|null...} stack
   */

  Rill.prototype.host = function() {
    var fn, host, stack;
    host = arguments[0], stack = 2 <= arguments.length ? slice.call(arguments, 1) : [];
    expect(typeof host === "string", "Host name must be a string.");
    expect(indexOf.call(host, "/") < 0, "Host path must not contain '/'.");
    fn = compose(stack);
    this.stack.push(function(req, res, next) {
      var lastIndex, pos;
      pos = req.hostname.length - host.length;
      lastIndex = req.hostname.indexOf(host, pos);
      if (req.hostname.indexOf(host, req.hostname.length - host.length) !== -1) {
        return fn.call(this, req, res, next);
      } else {
        return next;
      }
    });
    return this;
  };


  /*
  	 * Attaches a seperate rill application or middleware at a given path.
  	 * req.path and req.params will be updated to remove the provided path.
  	 *
  	 * @param {String} path
  	 * @param {Rill|Function|null...} stack
   */

  Rill.prototype.mount = function() {
    var fn, path, stack;
    path = arguments[0], stack = 2 <= arguments.length ? slice.call(arguments, 1) : [];
    expect(typeof path === "string", "Mount path must be a string.");
    expect(path[0] === "/", "Mount path must begin with '/'.");
    expect(path.slice(-1) !== "/", "Mount path must not end with '/'.");
    fn = compose(stack);
    this.stack.push(function(req, res, next) {
      var newPath, newRedirect, prevPath, prevRedirect;
      if (req.path.indexOf(path) !== 0) {
        return next;
      }
      prevRedirect = req.redirect;
      prevPath = req.path;
      newRedirect = req.redirect = mountRedirect(req, path);
      newPath = req.path = req.path.replace(path, "");
      return fn.call(this, req, res, {
        then: function(resolve, reject) {
          req.redirect = prevRedirect;
          req.path = prevPath;
          return next.then(function() {
            req.redirect = newRedirect;
            return req.path = newPath;
          }).then(resolve, reject);
        }
      }).then(function(result) {
        req.redirect = prevRedirect;
        req.path = prevPath;
        return result;
      });
    });
    return this;
  };


  /*
  	 * Creates methods for each HTTP verb and a "USE" method which will match all.
  	 *
  	 * @param {String} path
  	 * @param {Rill|Function|null...} stack
   */

  ref = ["AT", "OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE"];
  fn1 = function(method) {
    return Rill.prototype[method.toLowerCase()] = function() {
      var fn, keys, path, stack;
      path = arguments[0], stack = 2 <= arguments.length ? slice.call(arguments, 1) : [];
      expect(typeof path === "string", "Path must be a string.");
      expect(path[0] === "/", "At path must begin with '/'.");
      path = pathToReg(path, keys = []);
      fn = compose(stack);
      this.stack.push(function(req, res, next) {
        var i, k, len1, match, name, optional, ref1;
        if (!((method === "AT" || req.method === method) && (match = req.path.match(path)))) {
          return next;
        }
        if (req.params == null) {
          req.params = {};
        }
        for (i = k = 0, len1 = keys.length; k < len1; i = ++k) {
          ref1 = keys[i], name = ref1.name, optional = ref1.optional;
          if (!(((req.params[name] = match[i + 1]) != null) || optional)) {
            return next;
          }
        }
        return fn.call(this, req, res, next);
      });
      return this;
    };
  };
  for (j = 0, len = ref.length; j < len; j++) {
    method = ref[j];
    fn1(method);
  }

  return Rill;

})();

module.exports = Rill;
