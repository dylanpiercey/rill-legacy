/** Rill v0.0.906 https://www.npmjs.com/package/rill License: MIT*/
"use strict";
var Request, expect, parseURL;

parseURL = require("@rill/parse-url");

expect = require("./util/expect");


/*
 * Constructor that will normalize a request object.
 *
 * @param {Object} req
 * @constructor
 */

Request = (function() {
  function Request(req, ctx) {
    var key, ref, ref1, ref2, ref3, ref4, ref5, ref6, ref7, val;
    if (!(this instanceof Request)) {
      return new Request(req, ctx);
    }
    expect(req != null, "Could not create Request with an empty object.");
    expect(req.url != null, "Could not create a Request without a URL.");
    this._ctx = ctx;
    this.method = ((ref = req.method) != null ? ref : "GET").toUpperCase();
    this.headers = (ref1 = req.headers) != null ? ref1 : {};
    this.files = (ref2 = req.files) != null ? ref2 : [];
    this.body = (ref3 = req.body) != null ? ref3 : {};
    this.socket = req.socket;
    this.req = req;
    this.ip = (ref4 = (ref5 = this.headers["x-forwarded-for"]) != null ? ref5.split(",")[0] : void 0) != null ? ref4 : (ref6 = req.connection) != null ? ref6.remoteAddress : void 0;
    this.headers["referrer"] = this.headers["referer"];
    ref7 = parseURL(req.url, this);
    for (key in ref7) {
      val = ref7[key];
      this[key] = val;
    }
  }


  /*
  	 * Trigger another request that will redirect the browser and send a location change on the server.
  	 * If "back" is the url then it will attempt to redirect the user back a url, otherwise the alt will be used.
  	 *
  	 * @param {String} url
  	 * @param {String} alt
   */

  Request.prototype.redirect = function(url, alt) {
    if (url == null) {
      url = "back";
    }
    if (alt == null) {
      alt = "/";
    }
    return this.redirectURL = this._ctx.response.headers["location"] = (url !== "back" ? url : this.headers.referer || alt);
  };

  return Request;

})();

module.exports = Request;
