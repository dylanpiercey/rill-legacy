assert   = require("assert")
details  = require("../package.json")
Request  = require("../src/request.coffee")
defaults = (src, dest)-> dest[key] ?= val for key, val of src; dest

describe "#{details.name}@#{details.version} - Request", ->
	require("mocha-jsdom")() if typeof document is "undefined"

	it "should require a request object", ->
		Request.should.throw("Could not create Request with an empty object.")
		Request.bind(null, {}).should.throw("Could not create a Request without a URL.")
		Request({ url: "/test" }).should.be.instanceOf(Request)

	it "should use get as the default method", ->
		Request(url: "/test").should
			.have.property("method", "GET")

		Request(method: "POST", url: "/test").should
			.have.property("method", "POST")

	it "should accept headers", ->
		Request(url: "/test", headers: test: true).should
			.have.property("headers")
			.with.property("test", true)

	describe "server", ->
		reqData = null
		before -> process.browser = undefined
		beforeEach ->
			reqData =
				url: "/test"
				connection:
					remoteAddress: "127.0.0.1"
				headers:
					host:     "localhost:3002"
					referer: "http://google.ca"
					cookie:   "username = John%20Doe; expires=Thu%2C%2018%20Dec%202013%2012%3A00%3A00%20UTC"
				socket:
					encrypted: true

		it "should default to the host, port and protocol of the request", ->
			req = Request(reqData)
			assert.equal(req.ip, "127.0.0.1")
			assert.equal(req.host, "localhost:3002")
			assert.equal(req.hostname, "localhost")
			assert.equal(req.port, "3002")
			assert.equal(req.protocol, "https")


