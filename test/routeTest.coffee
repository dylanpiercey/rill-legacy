should  = require("should")
compose = require("@rill/compose")
details = require("../package.json")
# _       = require("../src/util/route")

describe.skip "#{details.name}@#{details.version} - Route", ->
	require("co-mocha")

	# Returns true if the route was handled.
	resolver = -> Promise.resolve(true)

	it "#method should check for valid methods", ->
		methods =
			OPTIONS: _.method("OPTIONS", [resolver])
			GET:     _.method("GET", [resolver])
			PUT:     _.method("PUT", [resolver])
			POST:    _.method("POST", [resolver])
			PUT:     _.method("PUT", [resolver])
			DELETE:  _.method("DELETE", [resolver])

		for method, handler of methods
			# Valid request.
			(yield handler({ method }))
				.should.equal(true)

			# Invalid request.
			should.not.exist(yield handler(method: "UNKNOWN"))

	it "#host should check for valid hostname", ->
		handler = _.host("test.com", [resolver])
		# Valid request.
		(yield handler(hostname: "test.com"))
			.should.equal(true)

		# Invalid request.
		should.not.exist(yield handler(hostname: "fake.com"))


	it "#path should check for valid path", ->
		handler = _.path("/test", [resolver])
		# Valid request.
		(yield handler(path: "/test"))
			.should.equal(true)

		(yield handler(path: "/test/"))
			.should.equal(true)

		# Invalid request.
		should.not.exist(yield handler(path: "/tes"))
		should.not.exist(yield handler(path: "/test/1"))

	it "#path should attach params", ->
		handler = _.path("/test/:id", [(req, res, next)->
			req.params.id.should.equal("123")
			Promise.resolve(true)
		])

		# Valid request.
		(yield handler(path: "/test/123"))
			.should.equal(true)

		(yield handler(path: "/test/123/"))
			.should.equal(true)

		# Invalid request.
		should.not.exist(yield handler(path: "/test"))
		should.not.exist(yield handler(path: "/test/"))

	it "#mount should mask the path to the current middleware", ->
		handler = _.mount("/test", [(req, res, next)->
			new Promise((resolve, reject)->
				req.path.should.equal("/mounted")
				next.then(->
					req.path.should.equal("/mounted")
					resolve(true)
				)

			)
		])

		# Valid request.
		(yield handler(path: "/test/mounted", {}, Promise.resolve()))
			.should.equal(true)

		# Invalid request.
		should.not.exist(yield handler(path: "/mounted", {}, Promise.resolve()))

		# Downstream should be unaffected.
		yield compose([handler, (req, res, next)->
			req.path.should.equal("/test/mounted")
			Promise.resolve(true)
		])(path: "/test/mounted")