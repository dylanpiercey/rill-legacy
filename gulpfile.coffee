fs          = require("fs")
gulp        = require("gulp")
header      = require("gulp-header")
isBrowser   = require("gulp-isbrowser")
coffee      = require("gulp-coffee")
mocha       = require("gulp-mocha")
details     = require("./package.json")
Stream      = require("stream")
{ merge }   = require("event-stream")
src         = "./src"
client      = "./client"
server      = "./server"

try
	fs.mkdirSync(__dirname + client)
	fs.mkdirSync(__dirname + server)

getStream = ->
	gulp.src("#{src}/**/*.coffee")
		.pipe(coffee(bare: true, join: true).on("error", handleError = (err)->
			console.log(err.toString())
			console.log(err.stack)
			this.emit("end")
		))
		.pipe(header("""
			/** Rill v#{details.version} https://www.npmjs.com/package/rill License: MIT*/
			"use strict";\n
		"""))

###
Build all local cs.
###
gulp.task("build", ->
	merge(
		getStream()
			.pipe(isBrowser(no))
			.pipe(gulp.dest(server))
		getStream()
			.pipe(isBrowser(yes))
			.pipe(gulp.dest(client))
	)
)

###
Run tests.
###
gulp.task("test", ->
	gulp.src("./test/**/*Test.coffee", read: false)
		.pipe(mocha())
)
