/** Rill v0.0.906 https://www.npmjs.com/package/rill License: MIT*/
"use strict";

/*
 * Constructor that will normalize a response object.
 *
 * @param {Object} res
 * @constructor
 */
var Response;

Response = (function() {
  function Response(res, ctx) {
    if (!(this instanceof Response)) {
      return new Response(res);
    }
    this._ctx = ctx;
    this.res = res;
    this.status = res.statusCode || res.status || 404;
    this.headers = res.headers = {};
    this.body = void 0;
  }

  Object.defineProperty(Response.prototype, "headersSent", {
    get: function() {
      return this.res.headersSent;
    }
  });


  /*
  	 * Convenience function to test if the original response has already been sent.
  	 * TODO: Phase out.
   */

  Response.prototype.isSent = function() {
    return Boolean(this.res.headersSent);
  };

  return Response;

})();

module.exports = Response;
