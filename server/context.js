/** Rill v0.0.906 https://www.npmjs.com/package/rill License: MIT*/
"use strict";
var Context, Cookies, HttpError, Request, Response;

HttpError = require("./util/httpError");

Request = require("./request");

Response = require("./response");

Cookies = require("@rill/cookies");


/*
 * Constructor that will create a context for a request.
 *
 * @param {Object} res
 * @constructor
 */

Context = (function() {
  function Context(request, response) {
    if (!(this instanceof Context)) {
      return new Context(request, response);
    }
    this.request = new Request(request, this);
    this.response = new Response(response, this);
    this.cookies = this.request.cookies = this.response.cookies = new Cookies(request.headers.cookie);
  }


  /*
  	 * Utility to throw an http error during a request.
   */

  Context.prototype["throw"] = function(code, message, meta) {
    var error;
    error = new HttpError(code, message, meta);
    this.response.status = error.code;
    throw error;
  };


  /*
  	 * Throws if a condition failts.
   */

  Context.prototype.assert = function(val, code, message, meta) {
    if (val) {
      return;
    }
    return this["throw"](code, message, meta);
  };

  return Context;

})();

module.exports = Context;
