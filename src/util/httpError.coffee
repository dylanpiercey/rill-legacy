{ STATUS_CODES } = require("@rill/http")

HttpError = (code, message, meta)->
	@code    = code
	@message = message ? STATUS_CODES[code]
	Error.call(this)
	Error.captureStackTrace?(this, HttpError)
	return

HttpError.prototype      = new Error
HttpError.prototype.name = "HttpError"

module.exports = HttpError