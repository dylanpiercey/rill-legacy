byteLength       = require("byte-length")
{ STATUS_CODES } = require("@rill/http")
isType           = require("@rill/is-type")
reg              =
	html:       /^\s*</,
	cookieOptions: /(domain|path|expires|max-age|httponly|secure)( *= *[^;]*)?/g,
	cookieValues:  / *; */

# status codes for redirects
redirectStatus = [
	"300"
	"301"
	"302"
	"303"
	"305"
	"307"
	"308"
]

# status codes for empty bodies
emptyStatus = [
	"204"
	"205"
	"304"
]

###
# A default middleware that handles sending the response for the request.
#
# @param {Object} ctx
# @param {Object} req
# @param {Object} res
###
module.exports = (ctx, req, res)->
	# Skip request if something had already ended it.
	return if res.headersSent
	{ res: originalRes, headers, status, statusText, body } = res

	# Persist cookies.
	if cookies = ctx.cookies.serialize()
		if process.browser
			options = (cookies.match(reg.cookieOptions) or []).join("; ")
			options = "; " + options if options
			cookies = cookies
				.replace(reg.cookieOptions, "")
				.split(reg.cookieValues)
				.filter(Boolean)
				.map((cookie)-> cookie + options)
		res.headers["set-cookie"] = cookies

	# Set redirect status automatically.
	if headers["location"]?
		status = 302 unless status in redirectStatus

	# Default the status to 200 if there is substance to the response.
	status      = 200 if Number(status) is 404 and body?
	statusText ?= STATUS_CODES[status]

	# Ensure appropriate content-type and length.
	switch
		when status in emptyStatus or not body?
			delete headers["content-type"]
			body = undefined

		when isType.String(body)
			headers["content-type"] ?= "text/#{if reg.html.test(body) then "html" else "plain"}; charset=UTF-8"

		when isType.Buffer(body)
			headers["content-length"] = body.length

		when isType.Stream(body)
			headers["content-type"] ?= "application/octet-stream"

		else try
			body                    = JSON.stringify(body)
			headers["content-type"] ?= "application/json; charset=UTF-8"

	originalRes.writeHead(status, statusText, headers)
	switch
		when "HEAD" is req.method
			originalRes.end()

		when isType.Stream(body)
			body.pipe(originalRes)

		else
			headers["content-length"] ?= byteLength(body)
			originalRes.end(body)
	return

