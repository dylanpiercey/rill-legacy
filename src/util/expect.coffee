class RillError extends Error
	name: "Rill-Error"
	constructor: (@message)->
		Error.call(this)
		Error.captureStackTrace?(@, module.exports)

###
# Simple utility that asserts that a condition is true, or throws an error with a message.
#
# @param {Boolean*} condition
# @param {String} message
###
module.exports = (condition, message)->
	throw new RillError(message) unless condition
