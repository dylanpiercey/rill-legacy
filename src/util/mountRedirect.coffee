###
# Utility to handle redirects inside mounted middleware.
#
# @param {Request} req
# @param {String} path
###
module.exports = (req, path)->
	{ redirect } = req
	(url = "back", alt = "/")->
		alt = path + alt if alt[0] is "/"
		url = path + url if url[0] is "/"
		redirect.call(req, url, alt)