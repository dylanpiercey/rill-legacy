parseURL = require("@rill/parse-url")
expect   = require("./util/expect")

###
# Constructor that will normalize a request object.
#
# @param {Object} req
# @constructor
###
class Request
	constructor: (req, ctx)->
		return new Request(req, ctx) unless this instanceof Request
		expect(req?, "Could not create Request with an empty object.")
		expect(req.url?, "Could not create a Request without a URL.")
		@_ctx                = ctx
		@method              = (req.method ? "GET").toUpperCase()
		@headers             = req.headers ? {}
		@files               = req.files ? []
		@body                = req.body ? {}
		@socket              = req.socket
		@req                 = req
		@ip                  = @headers["x-forwarded-for"]?.split(",")[0] ? req.connection?.remoteAddress
		@headers["referrer"] = @headers["referer"]
		@[key]               = val for key, val of parseURL(req.url, this)

	###
	# Trigger another request that will redirect the browser and send a location change on the server.
	# If "back" is the url then it will attempt to redirect the user back a url, otherwise the alt will be used.
	#
	# @param {String} url
	# @param {String} alt
	###
	redirect: (url="back", alt="/")->
		@redirectURL = @_ctx.response.headers["location"] = (
			unless url is "back" then url
			else @headers.referer or alt
		)

module.exports = Request
