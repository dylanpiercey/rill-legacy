HttpError = require("./util/httpError")
Request   = require("./request")
Response  = require("./response")
Cookies   = require("@rill/cookies")

###
# Constructor that will create a context for a request.
#
# @param {Object} res
# @constructor
###
class Context
	constructor: (request, response)->
		return new Context(request, response) unless this instanceof Context
		@request          = new Request(request, this)
		@response         = new Response(response, this)
		@cookies          =
		@request.cookies  =
		@response.cookies = new Cookies(request.headers.cookie)


	###
	# Utility to throw an http error during a request.
	###
	throw: (code, message, meta)->
		error            = new HttpError(code, message, meta)
		@response.status = error.code
		throw error

	###
	# Throws if a condition failts.
	###
	assert: (val, code, message, meta)->
		return if val
		@throw(code, message, meta)

module.exports = Context
