pathToReg     = require("path-to-regexp")
compose       = require("@rill/compose")
expect        = require("./util/expect")
respond       = require("./util/respond")
mountRedirect = require("./util/mountRedirect")
HttpError     = require("./util/httpError")
Context       = require("./context")
http          = require("@rill/http")

class Rill
	###
	# Creates an isomorphic Rill application that can listen for browser and server page requests.
	#
	# @constructor
	###
	constructor: ->
		return new Rill() unless this instanceof Rill
		@stack = []

	###
	# Starts the server or browser hijacker.
	###
	listen: ->
		fn = compose(@stack)
		server = http.createServer((req, res)->
			res.statusCode = 404
			context = { request, response } = new Context(req, res)

			fn.call(context, request, response)
				.then(-> respond(context, request, response))
				.catch((err)->
					console.log?("Rill: Unhandled error.")
					console.error?(err?.stack or err)
				)
		).listen(arguments...)

	###
	# Utility to throw an http error during a request.
	###
	@throw: (code, message, meta)->
		throw new HttpError(code, message, meta)

	###
	# Throws if a condition failts.
	###
	@assert: (val, code, message, meta)->
		return if val
		throw new HttpError(code, message, meta)

	###
	# Simple syntactic sugar for functions that wish to modify the current application instance.
	#
	# @param {Function...} transformers
	###
	setup: (transformers...)->
		transformer(@) for transformer in transformers when transformer?
		return this

	###
	# Append new middleware to the current rill application stack.
	#
	# @param {Rill|Function|null...} stack
	###
	use: (stack...)->
		@stack = @stack.concat(stack)
		return this

	###
	# Attaches a seperate rill application or middleware at a given hostname.
	#
	# @param {String} host
	# @param {Rill|Function|null...} stack
	###
	host: (host, stack...)->
		expect(typeof host is "string", "Host name must be a string.")
		expect("/" not in host, "Host path must not contain '/'.")
		fn = compose(stack)

		@stack.push((req, res, next)->
			pos       = req.hostname.length - host.length
			lastIndex = req.hostname.indexOf(host, pos)
			# Validate request host.
			if req.hostname.indexOf(host, req.hostname.length - host.length) isnt -1
				fn.call(@, req, res, next)
			else next
		)
		return this

	###
	# Attaches a seperate rill application or middleware at a given path.
	# req.path and req.params will be updated to remove the provided path.
	#
	# @param {String} path
	# @param {Rill|Function|null...} stack
	###
	mount: (path, stack...)->
		expect(typeof path is "string", "Mount path must be a string.")
		expect(path[0] is "/", "Mount path must begin with '/'.")
		expect(path[-1..] isnt "/", "Mount path must not end with '/'.")
		fn = compose(stack)

		@stack.push((req, res, next)->
			# Validate request path.
			return next unless req.path.indexOf(path) is 0
			# Overwrite existing path and redirect.
			prevRedirect = req.redirect
			prevPath     = req.path
			newRedirect  = req.redirect = mountRedirect(req, path)
			newPath      = req.path = req.path.replace(path, "")

			fn.call(@, req, res, then: (resolve, reject)->
				req.redirect = prevRedirect
				req.path     = prevPath
				next.then(->
					req.redirect = newRedirect
					req.path     = newPath
				).then(resolve, reject)
			).then((result)->
				req.redirect = prevRedirect
				req.path     = prevPath
				result
			)
		)
		return this

	###
	# Creates methods for each HTTP verb and a "USE" method which will match all.
	#
	# @param {String} path
	# @param {Rill|Function|null...} stack
	###
	for method in ["AT", "OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE"]
		do (method)-> Rill::[method.toLowerCase()] = (path, stack...)->
			expect(typeof path is "string", "Path must be a string.")
			expect(path[0] is "/", "At path must begin with '/'.")
			path = pathToReg(path, keys = [])
			fn   = compose(stack)

			@stack.push((req, res, next)->
				# Validate request method.
				return next unless (method is "AT" or req.method is method) and match = req.path.match(path)
				# Store new params in request.
				req.params ?= {}
				for { name, optional }, i in keys
					return next unless (req.params[name] = match[i + 1])? or optional

				fn.call(@, req, res, next)
			)
			return this

module.exports = Rill
