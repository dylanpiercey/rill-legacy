###
# Constructor that will normalize a response object.
#
# @param {Object} res
# @constructor
###
class Response
	constructor: (res, ctx)->
		return new Response(res) unless this instanceof Response
		@_ctx    = ctx
		@res     = res
		@status  = res.statusCode or res.status or 404
		@headers = res.headers = {}
		@body    = undefined

	# Defer sent headers to native response object.
	Object.defineProperty(@::, "headersSent", get: -> @res.headersSent)
	###
	# Convenience function to test if the original response has already been sent.
	# TODO: Phase out.
	###
	isSent: -> Boolean(@res.headersSent)

module.exports = Response
